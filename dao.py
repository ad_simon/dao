import time

import mysql.connector
import json

import flask
from flask import Flask, Response, request
from flask_cors import CORS

from datetime import datetime, timedelta

# from casestudy.dao.query_generator import *
from queries import *

app = Flask(__name__)
CORS(app)

mysql_url_str = '10.1.13.87'
mysql_port_str = '3306'
db_user = 'root'
db_password = 'teetan1'
db_name = 'TradeHistory'

db_connect = mysql.connector.connect(
    host=mysql_url_str,
    user=db_user,
    password=db_password,
    database=db_name
)

connector = db_connect.cursor()

def parse_json(json_str):
    json_str = json_str.replace('\'', '"')
    json_str = json_str.replace('\\', '')
    json_str = json_str.replace('/', '')

    return json.loads(json_str)

history = []



@app.route('/test')
def test():
    return 'test'


@app.route('/addDeal/<data>', methods=['GET'])
@app.route('/addDeal/', methods=['POST'], defaults={'data': None})
def add_deal(data):
    if flask.request.method == 'GET':
        json_data = parse_json(data)
    elif flask.request.method == 'POST':
        json_data = request.get_json()
        json_data = parse_json(json_data)
    try:
        connector.execute(get_inst_id_query(json_data['instrumentName']))
        inst_id = connector.fetchone()[0]

        connector.execute(get_cp_id_query(json_data['cpty']))
        cp_id = connector.fetchone()[0]

        connector.execute(insert_deal_query(json_data['deal_id'], cp_id, inst_id, json_data['type'],
                                            json_data['quantity'], json_data['price'], json_data['time']))

        db_connect.commit()

        history.append(json.dumps(json_data) + '\n')
        return Response('Successfully inserted:\n' + str(data), status=200)
    except:
        return ('smth wrong\n' + get_inst_id_query(json_data['instrumentName']) + '\n',
                + get_cp_id_query(json_data['cpty']) + '\n' + insert_deal_query(json_data['deal_id'], cp_id, inst_id, json_data['type'],
                                                                                    json_data['quantity'],
                                                                                    json_data['price'],
                                                                                    json_data['time']))


@app.route('/stream/')
def stream():
    def eventStream():
        while True:
            res = None
            time.sleep(3)
            try:
                res = history.pop(0)
            except IndexError:
                pass
            if res is None:
                continue
            yield res
    return Response(eventStream(), mimetype="text/event-stream")



@app.route('/mean/<data>')
@app.route('/mean/', methods=['POST'], defaults={'data': None})
def get_avg(data):

    json_data = {}
    start_time = None
    end_time = None
    if flask.request.method == 'GET':
        json_data = parse_json(data)
    elif flask.request.method == 'POST':
        json_data = request.get_json()
        json_data = parse_json(json_data)

    if 'start_time' in json_data.keys():
        start_time = json_data['start_time']
    if 'end_time' in json_data.keys():
        end_time = json_data['end_time']

    connector.execute(select_avg(start_time, end_time))
    buy = {}
    sell = {}

    for line in connector:
        if line[1] == 'B':
            cur = buy
        else:
            cur = sell

        cur[line[0]] = 0 if line[2] is None else str(line[2])

    return Response('{' + '\"buy\": {0}, "sell": {1}, "event": "sell_buy_avg" '.format(json.dumps(buy), json.dumps(sell)) + '}', status=200)


def json_format(obj):
    res = []
    for i in range(obj):
        res.append(str(obj[i]))
    return res

@app.route('/endProfit/', methods=['POST'])
def end_profit_info():

    json_data = request.get_json()
    json_data = parse_json(json_data)

    start_time = None
    end_time = None
    cp_id = None

    if 'start_time' in json_data.keys():
        start_time = json_data['start_time']
    if 'end_time' in json_data.keys():
        end_time = json_data['end_time']
    if 'cp_id' in json_data.keys():
        cp_id = int(json_data['cp_id'])

    connector.execute(select_ordertype_quantity_price(cp_id, start_time, end_time))

    header = ['order_type']
    if cp_id is not None:
        header.append('cp_id')
    header.append('quantity')
    header.append('price')
    result = [json_format(header)]
    for line in connector:
        result.append(json_format(line))

    return Response('{ "result": ' + json.dumps(result) + '}', status=200)


@app.route('/effectiveProfitInfo/', methods=['POST'])
def eff_profit_info():
    json_data = request.get_json()
    json_data = parse_json(json_data)

    start_time = None
    end_time = None
    cp_id = None

    if 'start_time' in json_data.keys():
        start_time = json_data['start_time']
    if 'end_time' in json_data.keys():
        end_time = json_data['end_time']
    if 'cp_id' in json_data.keys():
        cp_id = int(json_data['cp_id'])

    connector.execute(select_ordertype_quantity_price(cp_id, start_time, end_time, timestamp=True))
    header = ['order_type']
    if cp_id is not None:
        header.append('cp_id')
    header.append('quantity')
    header.append('price')
    header.append('timestamp')

    result = [json_format(header)]

    for line in connector:
        result.append(json_format(line))

    return Response('{ "result": ' + json.dumps(result) + '}', status=200)



@app.route('/positionInfo/', methods=['POST'])
def getPosition():
    json_data = request.get_json()
    json_data = parse_json(json_data)

    start_time = None
    end_time = None

    if 'start_time' in json_data.keys():
        start_time = json_data['start_time']
    if 'end_time' in json_data.keys():
        end_time = json_data['end_time']

    cp_id = int(json_data['cp_id'])
    inst_id = int(json_data['inst_id'])

    connector.execute(select_ordertype_quantity(cp_id, inst_id, start_time, end_time))

    header = ['order_type', 'quantity']
    result = [json_format(header)]
    for line in connector:
        result.append(json_format(line))

    return Response('{ "result": ' + json.dumps(result) + '}', status=200)


def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))


if __name__ == '__main__':
    bootapp()
