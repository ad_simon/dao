def __make_date_options__(start_time, end_time):
    start = ''
    end = ''
    if start_time is not None:
        start = ' timestamp >= \'{0}\' '.format(start_time)
    if end_time is not None:
        end = ' timestamp <= \'{0}\' '.format(end_time)
    if start_time is not None and end_time is not None:
        start += ' AND '
    return start + end


def get_inst_id_query(name):
    return "SELECT inst_id FROM instruments WHERE name = \"{}\"".format(name)


def get_cp_id_query(name):
    return "SELECT cp_id FROM counterparties WHERE name = \"{}\"".format(name)


def insert_deal_query(deal_id, cp_id, inst_id, order_type, quantity, price, timestamp):
    return 'INSERT INTO deals (deal_id, cp_id, inst_id, order_type, quantity, price, timestamp) ' \
           'VALUES( "{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}");' \
        .format(deal_id, cp_id, inst_id, order_type, quantity, price, timestamp)


def select_avg(start_time=None, end_time=None):
    time_option = __make_date_options__(start_time, end_time)

    return 'SELECT t2.name, t2.order_type, average_price ' \
           ' FROM ' \
           ' (SELECT avg(price) as average_price, inst_id, order_type ' \
           ' FROM deals ' \
           ' WHERE  {} ' \
           ' GROUP BY inst_id, order_type) as t1 ' \
           ' RIGHT JOIN ' \
           ' (SELECT inst_id, name, order_type ' \
           ' FROM instruments, order_types) as t2' \
           ' ON t2.inst_id = t1.inst_id AND t2.order_type = t1.order_type' \
        .format(time_option)


def select_last_inst_price(start_time, end_time, inst_id):
    time_option = __make_date_options__(start_time, end_time)

    return 'SELECT price FROM deals' \
           'WHERE {} AND inst_id = {}' \
           'ORDER BY timestamp DESC LIMIT 1'.format(time_option, inst_id)


def select_inst_sum_quantity(order_type, start_time, end_time):
    time_option = __make_date_options__(start_time, end_time)

    return 'SELECT SUM(quantity), inst_id FROM deals ' \
           'WHERE  {}  AND order_type = \'{}\' group by inst_id'.format(time_option, order_type)


